# search4letters

#### Files
 - The code of HTML pages in the 'templates' folder.
 - The css styles data is in the 'static' folder.
 - search4letters.log is the logging file of the webapp.
 - search4web.py is the main code of the app.

To start the program, you should launch it on the one of web-hosting services first. 
For instance, I used the one called "PythonAnywhere" (https://www.pythonanywhere.com).
You can watch on YouTube how to use it.

You can write to me using email: bigproject404@yandex.ru

I'd be glad to get any feedback or development ideas.
